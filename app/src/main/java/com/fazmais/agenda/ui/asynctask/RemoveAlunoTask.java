package com.fazmais.agenda.ui.asynctask;

import android.os.AsyncTask;

import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.ui.activity.ListaAlunosAdapter;

public class RemoveAlunoTask extends AsyncTask<Void, Void, Void> {
    private Aluno aluno;
    private AlunoDao dao;
    private ListaAlunosAdapter adapter;

    public RemoveAlunoTask(Aluno aluno, AlunoDao dao, ListaAlunosAdapter adapter) {
        this.aluno = aluno;
        this.dao = dao;
        this.adapter = adapter;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        dao.remove(aluno);
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        adapter.remove(aluno);
        super.onPostExecute(unused);
    }
}
