package com.fazmais.agenda.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.fazmais.agenda.R;
import com.fazmais.agenda.database.AgendaDataBase;
import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.database.dao.TelefoneDAO;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.model.Telefone;
import com.fazmais.agenda.model.TipoTelefone;
import com.fazmais.agenda.ui.asynctask.BuscaTodosTelefonesDoAlunoTask;
import com.fazmais.agenda.ui.asynctask.EditaAlunoTask;
import com.fazmais.agenda.ui.asynctask.SalvaAlunoTask;

import java.util.List;

import static com.fazmais.agenda.ui.activity.ConstantesActivities.CHAVE_ALUNO;


public class FormularioAlunoActivity extends AppCompatActivity {

    private static final String TITULO_APPBAR_NOVO_ALUNO = "Novo aluno";
    private static final String TITULO_APPBAR_EDITA_ALUNO = "Edita aluno";
    private EditText etCampoNome;
    private EditText etCampoTelefoneFixo;
    private EditText etCampoTelefoneCelular;
    private EditText etCampoEmail;
    private AlunoDao alunoDao;
    private TelefoneDAO telefoneDAO;
    private Aluno aluno;
    private List<Telefone> telefonesDoAluno;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_aluno);
        AgendaDataBase dataBase = AgendaDataBase.getInstance(this);
        alunoDao = dataBase.getAlunoDao();
        telefoneDAO = dataBase.getTelefoneDAO();
        inicializacaoDosCampos();
        recebendoIntente();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_formulario_aluno_menu_salvar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.activity_formulario_aluno_menu_salvar) {
            finalizaFormulario();
        }

        return super.onOptionsItemSelected(item);
    }

    private void recebendoIntente() {
        Intent dados = getIntent();
        if (dados.hasExtra(CHAVE_ALUNO)) {
            setTitle(TITULO_APPBAR_EDITA_ALUNO);
            aluno = (Aluno) dados.getSerializableExtra(CHAVE_ALUNO);
            preencheCampos();
        } else {
            setTitle(TITULO_APPBAR_NOVO_ALUNO);
            aluno = new Aluno();
        }
    }

    private void preencheCampos() {
        etCampoNome.setText(aluno.getNome());
        etCampoEmail.setText(aluno.getEmail());
        preencheCamposTelefone();


    }

    private void preencheCamposTelefone() {
        new BuscaTodosTelefonesDoAlunoTask(telefoneDAO, aluno, telefones -> {
            telefonesDoAluno = telefones;
            for (Telefone telefone : telefonesDoAluno) {
                if (telefone.getTipo() == TipoTelefone.FIXO) {
                    etCampoTelefoneFixo.setText(telefone.getNumero());
                } else {
                    etCampoTelefoneCelular.setText(telefone.getNumero());
                }
            }
        }).execute();

    }

    private void finalizaFormulario() {
        preencheAluno();
        Telefone telefoneFixo = criaTelefone(etCampoTelefoneFixo, TipoTelefone.FIXO);
        Telefone telefoneCelular = criaTelefone(etCampoTelefoneCelular, TipoTelefone.CELULAR);

        if (aluno.temIdValido()) {
            editaAluno(telefoneFixo, telefoneCelular);

        } else {
            salvaAluno(telefoneFixo, telefoneCelular);
        }

    }

    private Telefone criaTelefone(EditText etCampoTelefoneFixo, TipoTelefone telefone) {
        String numero = etCampoTelefoneFixo.getText().toString();
        return new Telefone(numero, telefone);
    }

    private void salvaAluno(Telefone telefoneFixo, Telefone telefoneCelular) {
        new SalvaAlunoTask(alunoDao, aluno, telefoneFixo, telefoneCelular, telefoneDAO, this::finish).execute();

    }



    private void editaAluno(Telefone telefoneFixo, Telefone telefoneCelular) {
        new EditaAlunoTask(telefoneFixo, telefoneCelular, aluno, alunoDao, telefoneDAO, telefonesDoAluno, this::finish).execute();

    }

    private void inicializacaoDosCampos() {
        etCampoNome = findViewById(R.id.activity_formulario_aluno_nome);
        etCampoTelefoneFixo = findViewById(R.id.activity_formulario_aluno_telefone_fixo);
        etCampoTelefoneCelular = findViewById(R.id.activity_formulario_aluno_telefone_celular);
        etCampoEmail = findViewById(R.id.activity_formulario_aluno_email);

    }


    private void preencheAluno() {
        String nome = etCampoNome.getText().toString();
        String email = etCampoEmail.getText().toString();
        aluno.setNome(nome);
        aluno.setEmail(email);
    }
}