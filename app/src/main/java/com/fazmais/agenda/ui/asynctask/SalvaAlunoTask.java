package com.fazmais.agenda.ui.asynctask;

import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.database.dao.TelefoneDAO;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.model.Telefone;

public class SalvaAlunoTask extends BaseAlunoComTelefoneTask {
    private final AlunoDao alunoDao;
    private final Aluno aluno;
    private final Telefone telefoneFixo;
    private final Telefone telefoneCelular;
    private final TelefoneDAO telefoneDAO;


    public SalvaAlunoTask(AlunoDao alunoDao,
                          Aluno aluno,
                          Telefone telefoneFixo, Telefone telefoneCelular,
                          TelefoneDAO telefoneDAO, FinalizadaListener listener) {

        super(listener);
        this.alunoDao = alunoDao;
        this.aluno = aluno;
        this.telefoneFixo = telefoneFixo;
        this.telefoneCelular = telefoneCelular;
        this.telefoneDAO = telefoneDAO;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        int alunoId = alunoDao.salva(aluno).intValue();
        vinculaAlunoComTelefone(alunoId, telefoneFixo, telefoneCelular);
        verificaSepossuiNumeroCadastrado(telefoneFixo, telefoneCelular);
        return null;
    }


    private void verificaSepossuiNumeroCadastrado(Telefone telefoneFixo, Telefone telefoneCelular) {
        if (telefoneFixo.getNumero().length() > 0) {
            telefoneDAO.salvar(telefoneFixo);
        }
        if (telefoneCelular.getNumero().length() > 0) {
            telefoneDAO.salvar(telefoneCelular);
        }
    }


}
