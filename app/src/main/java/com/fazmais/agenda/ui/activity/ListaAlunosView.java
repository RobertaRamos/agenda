package com.fazmais.agenda.ui.activity;

import android.content.Context;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;

import com.fazmais.agenda.database.AgendaDataBase;
import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.ui.asynctask.BuscaAlunosTask;
import com.fazmais.agenda.ui.asynctask.RemoveAlunoTask;

public class ListaAlunosView {
    private final Context context;
    private AlunoDao dao;
    private ListaAlunosAdapter adapter;


    public ListaAlunosView(Context context) {
        this.context = context;
        dao = AgendaDataBase.getInstance(context).getAlunoDao();

    }

    public void configuraAdapter(ListView lvListaAlunos) {
        adapter = new ListaAlunosAdapter(context);
        lvListaAlunos.setAdapter(adapter);
    }

    public void atualizaAlunos() {
        new BuscaAlunosTask(dao, adapter).execute();


    }

    private void remove(Aluno aluno) {
        new RemoveAlunoTask(aluno, dao, adapter).execute();

    }

    public void confirmaRemocao(final MenuItem item) {
        new AlertDialog.Builder(context)
                .setTitle("Removendo Aluno")
                .setMessage("Tem certeza que deseja remover o aluno?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                    Aluno alunoEscolhido = adapter.getItem(menuInfo.position);
                    remove(alunoEscolhido);

                })
                .setNegativeButton("Não", null)
                .show();
    }

}
