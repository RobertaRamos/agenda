package com.fazmais.agenda.ui.asynctask;

import android.os.AsyncTask;

import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.ui.activity.ListaAlunosAdapter;

import java.util.List;

public class BuscaAlunosTask extends AsyncTask<Void, Void, List<Aluno>>{
    private AlunoDao dao;
    private ListaAlunosAdapter adapter;

    public BuscaAlunosTask(AlunoDao dao, ListaAlunosAdapter adapter) {
        this.dao = dao;
        this.adapter = adapter;
    }

    @Override
    protected List<Aluno> doInBackground(Void[] Object) {
        return dao.todos();
    }

    @Override
    protected void onPostExecute(List<Aluno> alunos) {
        super.onPostExecute(alunos);
        adapter.atualiza(alunos);
    }
}
