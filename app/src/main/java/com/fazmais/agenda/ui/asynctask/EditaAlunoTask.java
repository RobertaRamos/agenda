package com.fazmais.agenda.ui.asynctask;

import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.database.dao.TelefoneDAO;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.model.Telefone;
import com.fazmais.agenda.model.TipoTelefone;

import java.util.List;

public class EditaAlunoTask extends BaseAlunoComTelefoneTask {
    private final AlunoDao alunoDao;
    private final Aluno aluno;
    private final Telefone telefoneFixo;
    private final Telefone telefoneCelular;
    private final TelefoneDAO telefoneDAO;
    private final List<Telefone> telefonesDoAluno;


    public EditaAlunoTask(Telefone telefoneFixo,
                          Telefone telefoneCelular,
                          Aluno aluno, AlunoDao alunoDao,
                          TelefoneDAO telefoneDAO, List<Telefone> telefonesDoAluno, FinalizadaListener listener) {

        super(listener);
        this.telefoneFixo = telefoneFixo;
        this.telefoneCelular = telefoneCelular;
        this.aluno = aluno;
        this.alunoDao = alunoDao;
        this.telefoneDAO = telefoneDAO;
        this.telefonesDoAluno = telefonesDoAluno;

    }

    @Override
    protected Void doInBackground(Void... voids) {
        alunoDao.edita(aluno);
        vinculaAlunoComTelefone(aluno.getId(), telefoneFixo, telefoneCelular);
        atualizaTelefones(telefoneFixo, telefoneCelular);

        return null;
    }

    private void atualizaTelefones(Telefone telefoneFixo, Telefone telefoneCelular) {
        alualizaIds(telefoneFixo, telefoneCelular);
        verificaNumero(telefoneFixo);
        verificaNumero(telefoneCelular);
    }

    private void verificaNumero(Telefone telefone) {
        if (telefone.getNumero().isEmpty() && telefone.getId() != 0) {
            telefoneDAO.remove(telefone);
        } else if (!telefone.getNumero().isEmpty() ){
            telefoneDAO.atualiza(telefone);
        }
    }

    private void alualizaIds(Telefone telefoneFixo, Telefone telefoneCelular) {
        for (Telefone telefone : telefonesDoAluno) {
            if (telefone.getTipo() == TipoTelefone.FIXO) {
                telefoneFixo.setId(telefone.getId());
            } else {
                telefoneCelular.setId(telefone.getId());
            }
        }
    }


}
