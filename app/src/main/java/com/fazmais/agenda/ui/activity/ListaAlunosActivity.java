package com.fazmais.agenda.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.fazmais.agenda.R;
import com.fazmais.agenda.model.Aluno;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.fazmais.agenda.ui.activity.ConstantesActivities.CHAVE_ALUNO;

public class ListaAlunosActivity extends AppCompatActivity {

    private static final String TITULO_APPBAR = "Lista de alunos";
    private ListaAlunosView listaAlunosView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos);
        setTitle(TITULO_APPBAR);
        listaAlunosView = new ListaAlunosView(this);
        configuraFabNovoAluno();
        configuraLista();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_lista_alunos_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.activity_lista_aluno_menu_remover) {
            listaAlunosView.confirmaRemocao(item);
        }
        return super.onContextItemSelected(item);
    }


    private void configuraFabNovoAluno() {
        FloatingActionButton botaoAdicionar = findViewById(R.id.activity_lista_aluno_fab_novo_aluno);
        botaoAdicionar.setOnClickListener(view -> abreFormularioModoInsereAluno());
    }

    private void abreFormularioModoInsereAluno() {
        startActivity(new Intent(this, FormularioAlunoActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Roberta", "onResume");
        listaAlunosView.atualizaAlunos();

    }

    private void configuraLista() {
        ListView lvListaAlunos = findViewById(R.id.activity_lista_de_alunos_listview);
        listaAlunosView.configuraAdapter(lvListaAlunos);
        configuraListinerDeCliquePorItem(lvListaAlunos);
        registerForContextMenu(lvListaAlunos);

    }

    private void configuraListinerDeCliquePorItem(ListView lvListaAlunos) {
        lvListaAlunos.setOnItemClickListener((adapterView, view, posicao, id) -> {
            Aluno alunoSelecionado = (Aluno) adapterView.getItemAtPosition(posicao);
            abreFormularioModoEditaAluno(alunoSelecionado);
        });
    }

    private void abreFormularioModoEditaAluno(Aluno alunoSelecionado) {
        Intent vaiParaFormularioActivity = new Intent(this, FormularioAlunoActivity.class);
        vaiParaFormularioActivity.putExtra(CHAVE_ALUNO, alunoSelecionado);
        startActivity(vaiParaFormularioActivity);
    }

}
