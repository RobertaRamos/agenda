package com.fazmais.agenda.ui.asynctask;

import android.os.AsyncTask;

import com.fazmais.agenda.database.dao.TelefoneDAO;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.model.Telefone;

import java.util.List;

public class BuscaTodosTelefonesDoAlunoTask extends AsyncTask<Void, Void, List<Telefone>> {
    private TelefoneDAO telefoneDAO;
    private Aluno aluno;
    private TelefonesDoAlunoEncontradosListener listener;

    public BuscaTodosTelefonesDoAlunoTask(TelefoneDAO telefoneDAO, Aluno aluno, TelefonesDoAlunoEncontradosListener listener) {

        this.telefoneDAO = telefoneDAO;
        this.aluno = aluno;
        this.listener = listener;
    }

    @Override
    protected List<Telefone> doInBackground(Void... voids) {
        return telefoneDAO.buscaTodosTelefonesDoAluno(aluno.getId());
    }

    @Override
    protected void onPostExecute(List<Telefone> telefones) {
        super.onPostExecute(telefones);
        listener.quandoEncontrado(telefones);
    }

    public interface TelefonesDoAlunoEncontradosListener{
        void quandoEncontrado(List<Telefone> telefones);
    }
}
