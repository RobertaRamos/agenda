package com.fazmais.agenda.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.fazmais.agenda.database.converter.ConversorCalendadr;
import com.fazmais.agenda.database.converter.ConversorTipoTelefone;
import com.fazmais.agenda.database.dao.AlunoDao;
import com.fazmais.agenda.database.dao.TelefoneDAO;
import com.fazmais.agenda.model.Aluno;
import com.fazmais.agenda.model.Telefone;

import static com.fazmais.agenda.database.AgendaMigrations.TODAS_MIGRATIONS;

@Database(entities = {Aluno.class, Telefone.class},version = 6,exportSchema = false)
@TypeConverters({ConversorCalendadr.class, ConversorTipoTelefone.class})
public abstract class AgendaDataBase extends RoomDatabase {

    private static final String NOME_BANCO_DE_DADOS = "agenda.db";

    public abstract TelefoneDAO getTelefoneDAO();
    public abstract AlunoDao getAlunoDao();

    public static AgendaDataBase getInstance(Context context){
       return
               Room.databaseBuilder(context, AgendaDataBase.class, NOME_BANCO_DE_DADOS)
                       .addMigrations(TODAS_MIGRATIONS).build();
    }




}


