package com.fazmais.agenda.model;

public enum TipoTelefone {

    FIXO, CELULAR
}
